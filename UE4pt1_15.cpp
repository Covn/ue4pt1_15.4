﻿#include <iostream>
void task(int N, bool ODD)
{
    for (int i = 0; i < ((N + N % 2) / 2); ++i)
    {
        std::cout << i * 2 + ODD<<"\n";
    }

}

int main()
{
    int n;
    bool odd;
    std::cin >> n;
    std::cin >> odd;
    task(n, odd);
}